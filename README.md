# gg-admin-theme

Default admin template for the projects in Green Global.

# Usage

### AdminLTE 2.3.3 original version

Download latest version in your favorite compress format:

- [.zip](https://bitbucket.org/toancauxanh/gg-admin-theme/get/master.zip)
- [.gz](https://bitbucket.org/toancauxanh/gg-admin-theme/get/master.tar.gz)
- [.bz2](https://bitbucket.org/toancauxanh/gg-admin-theme/get/master.tar.bz2)

Or clone repo:

```
git clone git@bitbucket.org:toancauxanh/gg-admin-theme.git
```

Then you can use starter.html as skeleton. See index.html and index2.html to get overview about the available components.

While the original version was built on top of Bootstrap and jQuery, there are several variants that have been customized to work with the particular frameworks such as Angular, React, Meteor... They would be listed below.

### AdminLTE for React

- [Download as .zip](https://bitbucket.org/toancauxanh/gg-admin-theme/get/react.zip)
- [Download as .gz](https://bitbucket.org/toancauxanh/gg-admin-theme/get/react.tar.gz)
- [Download as .bz2](https://bitbucket.org/toancauxanh/gg-admin-theme/get/react.tar.bz2)

Or clone repo at "react" branch:

```
git clone --depth 1 git@bitbucket.org:toancauxanh/gg-admin-theme.git -b react
cd gg-admin-theme
npm install
```

The output would be located at http:127.0.0.1:8000, open it with your browser.


### AdminLTE for Angular

// Coming soon

### AdminLTE for Meteor

// Coming soon


# Documentation

There is offline version included within any copy. Othewise, you can read it online here:

[AdminLTE documentation](https://almsaeedstudio.com/themes/AdminLTE/documentation/index.html)


# Screenshot

![AdminLTE starter.html - screenshot](http://i.imgur.com/C70clW6.png)


# READ-ONLY

Please don't commit source code here.
